// ==UserScript==
// @name         PokeClicker helper
// @namespace    https://gitlab.com/Hiroa/pokeclicker-helper/
// @version      0.17.1
// @description  Multi tool script for pokéclicker (0.10.9)
// @author       Hiroa
// @updateURL    https://gitlab.com/Hiroa/pokeclicker-helper/-/raw/main/pokeclicker-helper.user.js
// @downloadURL  https://gitlab.com/Hiroa/pokeclicker-helper/-/raw/main/pokeclicker-helper.user.js
// @match        https://www.pokeclicker.com/*
// @icon         https://www.pokeclicker.com/assets/images/favicon.ico
// @require	     https://cdn.jsdelivr.net/npm/underscore@1.13.2/underscore-umd-min.js
// @grant        none
// ==/UserScript==

const moduleName = 'pokeClickerHelper';

const capitalize = (word) => word.charAt(0).toUpperCase() + word.slice(1);
class Config {
    static get default() {
        return {
            intervalSpeed: 500,
            clickerIntervalSpeed: 50,
            autoBreeding: false,
            autoUnderground: false,
            autoClicker: false,
            autoGym: false,
            autoDungeon: false,
            breedingOrder: [],
            breedOnlyNonShiny: false,
            surveyFirst: false,
            displayRoaming: true,
            removeCatchTimer: false,
            autoRoute: false,
            dungeonClearChestFirst: false,
            dungeonClearFightFirst: false,
            routeMaxDefeated: 10000,
            routeOverride: false,
            routeMinHaltRegion: -1,
            routeMinHaltPrice: 0
        };
    }

    static get storageKey() { return moduleName; }

    static load() {
        let config = window.localStorage.getItem(this.storageKey);
        if (!config) {
            this.save(this.default);
            return this.default;
        }
        return Object.assign(this.default, JSON.parse(config));
    }

    static save(config) {
        window.localStorage.setItem(this.storageKey, JSON.stringify(config));
    }
}
class Breeding {

    static findPokemon(id) {
        return _.find(App.game.party.caughtPokemon, function (pokemon) {
            return pokemon.id === id;
        });
    }

    static validateInput(input) {
        let valid = true;
        let list = input.trim().split(',')
        list.every((number) => {
            let id = Number(number);
            if (PokemonHelper.getPokemonById(id).name === PokemonHelper.getPokemonById(0).name) {
                valid = false;
            }
            return valid;
        })
        return valid;
    }

    static run(config) {
        if (!config.autoBreeding) {
            return;
        }
        // let kantoHatcheryPriority = [130, 36, 40, 26, 35, 103, 59, 115, 128, 126, 89, 80, 114, 125, 127, 137, 123, 42, 55, 25, 39, 143, 108, 131, 110, 112, 22, 73, 38, 78, 97, 77, 121, 87, 83, 119, 91, 124, 76, 58, 49, 142];
        // let johtoHatcheryPriority = [184,234,203,241,206,176,233,217,199,224,211,164,232,214,229,222,198,168,210,226,221,171,185,193,192,130,195,207,169,205,162,178,200,215,227,202,173,174,160,181,230,212,216,240,157,237,231,219,154,186];
        // let hoennHatcheryPriority = [289,336,324,130,313,314,342,335,352,357,350,286,327,359,337,338,284,332,323,340,367,288,358,368,262,317,315,362,297,277,275,334,311,351,310,319,303,260,344,264,341,257,365,312,331,272,326];
        let maxSlot = App.game.breeding.eggSlots;
        for (let i = 0; i < maxSlot; i++) {
            let egg = App.game.breeding.eggList[i]();
            if (egg.progress() >= 100) {
                App.game.breeding.hatchPokemonEgg(i)
            }
            if (egg.type === -1) {
                let breedingId = config.breedingOrder.find((id) => {
                    let pokemon = Breeding.findPokemon(id);
                    if (pokemon === undefined || (config.breedOnlyNonShiny && pokemon.shiny)) {
                        return false;
                    }
                    return !pokemon.breeding && pokemon.level === 100;
                });
                if (breedingId !== undefined) {
                    let pokemon = this.findPokemon(breedingId);
                    App.game.breeding.addPokemonToHatchery(pokemon);
                }
            }
        }
    }
}
class Underground {
    static run(config) {
        if (!config.autoUnderground) {
            return;
        }
        if (App.game.underground.energy + this.energyGain() > App.game.underground.getMaxEnergy()) {
            if (config.surveyFirst && !Mine.surveyResult()) {
                Mine.survey();
            } else {
                Mine.bomb();
            }
        }
    }

    static energyGain() {
        let oakMultiplier = App.game.oakItems.calculateBonus(OakItemType.Cell_Battery);
        return oakMultiplier * App.game.underground.getEnergyGain();
    }
}
class Roaming {

    static run(config) {
        if (this.route !== RoamingPokemonList.increasedChanceRoute[player.region][0]().number) {
            this.route = RoamingPokemonList.increasedChanceRoute[player.region][0]().number;
            $(`#${UI.makeId('RoamingLocation')}`).text('Boosted roaming at route ' + this.route);
        }
        this.toggleDisplay(config);
    }

    static moveToRoaming() {
        if (this.route) {
            MapHelper.moveToRoute(RoamingPokemonList.increasedChanceRoute[player.region][0]().number, player.region);
        }
    }

    static toggleDisplay(config) {
        $(`#${UI.makeId('RoamingLocation')}`).toggle(config.displayRoaming);
    }
}class Route {

    static sortedGymList;

    static run(config) {
        if (!config.autoRoute || this.getDefeatedOnRoute(player.region, player.route()) < config.routeMaxDefeated) {
            return;
        }

        let route = this.getNextRoute(config);
        if (route) {
            MapHelper.moveToRoute(route.number, route.region);
        }
    }

    static shouldMoveToNextDungeon(config) {
        if (config.autoRoute) {
            if (player.town().dungeon !== undefined) {
                return this.getDefeatedOnDungeon(player.town().dungeon.name) >= config.routeMaxDefeated;
            } else if (this.getTownDungeon().length >= 1) {
                return this.getDefeatedOnDungeon(this.getTownDungeon()[0]) >= config.routeMaxDefeated;
            }
        }
        return false;
    }

    static moveToNextDungeon(config) {
        let dungeonName = this.getNextDungeonName(config);
        if (dungeonName) {
            MapHelper.moveToTown(dungeonName);
            player.region = TownList[dungeonName].region
        }
    }

    static shouldMoveToNextGym(currentGym, config) {
        if (config.autoRoute) {
            return this.getDefeatedOnGym(currentGym) >= config.routeMaxDefeated;
        }
        return false;
    }

    static moveToNextGym(config) {
        let gym = this.getNextGym(config);
        if (gym) {
            MapHelper.moveToTown(gym.parent.name);
            player.region = gym.parent.region
            this.handleAlolaSubRegion(gym)
            //Update selector
            let gymList = _.filter(gym.parent.content, c => {
                return c.badgeReward !== undefined
            });
            if (gymList.length > 1) {
                let index = _.findIndex(gymList, {town: gym.town});
                $(`#${UI.makeId('selectedGym')}`).val(index);
            }
        }
    }

    static getNextRoute(config) {
        let route = this.getNextRegionalRoute(player.region, config);
        if (route && MapHelper.accessToRoute(route.number, route.region)) {
            return route;
        }
        for (let i = 0; i <= player.highestRegion(); i++) {
            if (i === player.region) {
                continue;
            }
            let route = this.getNextRegionalRoute(i, config);
            if (route && MapHelper.accessToRoute(route.number, route.region)) {
                return route;
            }
        }
        return undefined;
    }

    static getNextGym(config) {
        let gym = this.getNextRegionalGym(player.region, config);
        if (gym && MapHelper.accessToTown(gym.parent.name)) {
            return gym;
        }
        for (let regionId = 0; regionId <= player.highestRegion(); regionId++) {
            if (regionId === player.region) {
                continue;
            }
            let gym = this.getNextRegionalGym(regionId, config);
            if (gym && MapHelper.accessToTown(gym.parent.name)) {
                return gym;
            }
        }
        return undefined;
    }

    static getNextDungeonName(config) {
        let dungeonName = this.getNextRegionalDungeon(player.region, config);
        if (dungeonName && MapHelper.accessToTown(dungeonName)) {
            return dungeonName;
        }
        for (let i = 0; i <= player.highestRegion(); i++) {
            if (i === player.region) {
                continue;
            }
            let dungeonName = this.getNextRegionalDungeon(i, config);
            if (dungeonName && MapHelper.accessToTown(dungeonName)) {
                return dungeonName;
            }
        }
        return undefined;
    }

    static getNextRegionalRoute(region, config) {
        let regionRoutes = Routes.getRoutesByRegion(region);
        return _.find(regionRoutes, (regionRoute) => this.getDefeatedOnRoute(region, regionRoute.number) < config.routeMaxDefeated);
    }

    static getNextRegionalGym(region, config) {
        if (!this.sortedGymList) {
            this.sortedGymList = _.indexBy(GymList, 'badgeReward')
        }
        let regionalGym = _.filter(this.sortedGymList, g => {
            return g.parent !== undefined && g.parent.region === region
        })
        return _.find(regionalGym, (gym) => this.getDefeatedOnGym(gym) < config.routeMaxDefeated && !gym.town.includes('Trial'));
    }

    static getNextRegionalDungeon(region, config) {
        let regionalDungeonName = GameConstants.RegionDungeons[region];
        return _.find(regionalDungeonName, (dungeonName) => this.getDefeatedOnDungeon(dungeonName) < config.routeMaxDefeated);
    }

    static getDefeatedOnRoute(region, route) {
        return App.game.statistics.routeKills[region][route]();
    }

    static getDefeatedOnGym(gym) {
        return App.game.statistics.gymsDefeated[GameConstants.getGymIndex(gym.town)]();
    }

    static getDefeatedOnDungeon(dungeonName) {
        return App.game.statistics.dungeonsCleared[GameConstants.getDungeonIndex(dungeonName)]();
    }

    static getCurrentTownGym() {
        let gymList = player.town().content.filter(c => {
            return c.badgeReward !== undefined
        });
        if (gymList.length === 1) {
            return gymList[0]
        } else if (gymList.length > 1) {
            let gymIndex = $(`#${UI.makeId('selectedGym')}`).val()
            return gymList[gymIndex];
        }
    }

    static getTownDungeon() {
        return player.town().content.filter(c => {
            return c.dungeon !== undefined
        }).map(c => {
            return c.dungeon
        });
    }

    static handleAlolaSubRegion(gym) {
        if (player.region === GameConstants.Region.alola) {
            let gymIndex = GameConstants.AlolaGyms.findIndex((name) => name === gym.town);
            if (gymIndex < 5) { //MelemeleIsland
                player.subregion = AlolaSubRegions.MelemeleIsland;
            } else if (gymIndex === 5) { //AkalaIsland
                player.subregion = AlolaSubRegions.AkalaIsland;
            } else if (gymIndex > 6) { //UlaulaAndPoniIslands
                player.subregion = AlolaSubRegions.UlaulaAndPoniIslands;
            } else if (gymIndex === 6 && player.subregion === 0) { //Aether Paradise
                player.subregion = AlolaSubRegions.AkalaIsland;
            }
            MapHelper.moveToTown(gym.parent.name); //Move again to counter the subregion change
        }
    }
}class Clicker {

    static timerId = undefined;
    static halted = false;

    static run(config) {
        if (config.autoClicker && !this.timerId) {
            clearInterval(this.timerId);
            this.timerId = setInterval(() => this.interval(config), config.clickerIntervalSpeed > 10 ? config.clickerIntervalSpeed : 50);
        } else if (!config.autoClicker && this.timerId) {
            clearInterval(this.timerId);
            this.timerId = undefined;
        }
    }

    static interval(config) {
        this.checkPreviousDungeonRun(config);
        switch (App.game.gameState) {
            case GameConstants.GameState.temporaryBattle:
                this.temporaryFighting()
                break;
            case GameConstants.GameState.fighting:
                this.fighting();
                break;
            case GameConstants.GameState.dungeon:
                this.removeHalt();
                this.dungeon(config);
                break;
            case GameConstants.GameState.gym:
                this.gym();
                break;
            case GameConstants.GameState.town:
                this.town(config);
                break;
        }
    }

    static fighting() {
        Battle.clickAttack();
    }

    static temporaryFighting() {
        TemporaryBattleBattle.clickAttack()
    }

    static dungeon(config) {
        if (!(DungeonRunner.fighting() || DungeonBattle.catching()) && config.autoDungeon) {
            DungeonCrawler.moveToNextTile(config)
        }
        switch (DungeonRunner.map.currentTile().type()) {
            case GameConstants.DungeonTile.boss:
                if ((config.dungeonClearChestFirst && DungeonCrawler.chestsTiles.length !== 0) ||
                    (config.dungeonClearFightFirst && DungeonCrawler.enemisTiles.length !== 0)) {
                    break;
                }
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.chest:
                if (config.autoDungeon && DungeonCrawler.bossTile.isVisible && !config.dungeonClearChestFirst) {
                    break;
                }
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.enemy:
            case GameConstants.DungeonTile.ladder:
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.empty:
                if (DungeonRunner.fighting()) {
                    DungeonRunner.handleClick();
                }
        }
    }

    static gym() {
        GymBattle.clickAttack();
    }

    static town(config) {
        //Auto Gym
        if (config.autoGym) {
            let gym = Route.getCurrentTownGym()
            if (gym) {
                if (Route.shouldMoveToNextGym(gym, config)) {
                    Route.moveToNextGym(config);
                } else {
                    GymRunner.startGym(gym);
                }
            }
        }

        //Auto Dungeon
        if (config.autoDungeon) {
            if (!this.halted && (player.town().dungeon || Route.getTownDungeon().length >= 1) && this.hasMoneyForTheDungeon()) {
                if (Route.shouldMoveToNextDungeon(config)) {
                    Route.moveToNextDungeon(config);
                } else {
                    if (player.town().dungeon) {
                        DungeonRunner.initializeDungeon(player.town().dungeon);
                    } else {
                        DungeonRunner.initializeDungeon(Route.getTownDungeon()[0]);
                    }
                }
            }
        }
    }

    static hasMoneyForTheDungeon() {

        if (player.town().dungeon) {
            return App.game.wallet.hasAmount(new Amount(player.town().dungeon.tokenCost, GameConstants.Currency.dungeonToken));
        } else {
            return App.game.wallet.hasAmount(new Amount(Route.getTownDungeon()[0].tokenCost, GameConstants.Currency.dungeonToken));
        }
    }

    static checkPreviousDungeonRun(config) {
        if (config.routeOverride) {
            return;
        }
        if (player.region <= config.routeMinHaltRegion) {
            return;
        }
        if (DungeonRunner.dungeon && DungeonRunner.dungeon.tokenCost < config.routeMinHaltPrice) {
            return;
        }
        //Check if previous run was successful
        if (config.autoDungeon && DungeonRunner.map && DungeonRunner.map.board() && DungeonRunner.dungeonFinished() && DungeonCrawler.bossStillAlive()) {
            let button = $(`#${UI.makeId('autoDungeon')}`);
            button.addClass('btn-danger');
            button.text(button.text().trim().replace('ON', 'HALTED'));
            this.halted = true;
        }
    }

    static removeHalt() {
        if (!this.halted) {
            return;
        }
        this.halted = false;
        let button = $(`#${UI.makeId('autoDungeon')}`);
        button.removeClass('btn-danger');
        button.text(button.text().trim().replace('HALTED', 'ON'));
    }
}
class DungeonCrawler {

    static bossStillAlive() {
        return DungeonRunner.map.board()[DungeonRunner.map.playerPosition().floor].findIndex(line =>
            line.find(tile => {
                return tile.type() === GameConstants.DungeonTile.boss || tile.type() === GameConstants.DungeonTile.ladder
            })) !== -1
    }

    static moveToNextTile(config) {
        this.floor = DungeonRunner.map.playerPosition().floor
        this.map = DungeonRunner.map.board()[this.floor];
        this.chestsTiles = [];
        this.enemisTiles = [];
        this.bossTile = undefined;
        this.lookForTilesOfInterest();
        if (!this.bossTile) {
            return
        }
        let path;
        if (this.bossTile.isVisible
            && (!config.dungeonClearChestFirst || this.chestsTiles.length === 0)
            && (!config.dungeonClearFightFirst || this.enemisTiles.length === 0)) {
            path = this.dijkstra(this.bossTile.point)[0].point;
        } else if (config.dungeonClearChestFirst && this.chestsTiles.filter(t => t.isVisible).length !== 0) {
            path = this.chestsTiles.filter(t => t.isVisible)
                .map(chest => this.dijkstra(chest.point))
                .sort(((a, b) => a[0].d - b[0].d))[0][0].point;
        } else if (config.dungeonClearFightFirst && this.enemisTiles.filter(t => t.isVisible).length !== 0) {
            path = this.enemisTiles.filter(t => t.isVisible)
                .map(enemy => this.dijkstra(enemy.point))
                .sort(((a, b) => a[0].d - b[0].d))[0][0].point;
        } else {
            path = this.nextUnvisitedAccessibleTile();
        }
        if (path) {
            DungeonRunner.map.moveToCoordinates(path.x, path.y, this.floor);
        }
    }

    static nextUnvisitedAccessibleTile() {
        for (let y = this.map.length - 1; y >= 0; y--) {
            for (let x = this.map.length - 1; x >= 0; x--) {
                if (!this.map[y][x].isVisited && DungeonRunner.map.hasAccessToTile(new Point(x, y, this.floor))) {
                    return new Point(x, y, this.floor);
                }
            }
        }
    }

    static lookForTilesOfInterest() {
        this.chestsTiles = [];
        this.enemisTiles = [];
        this.map.forEach((line, y) => {
            line.forEach((tile, x) => {
                //Mark all tiles
                if (!tile.point) {
                    tile.point = new Point(x, y, this.floor);
                }
                if (tile.type() === GameConstants.DungeonTile.chest) {
                    this.chestsTiles.push(tile);
                } else if (tile.type() === GameConstants.DungeonTile.enemy) {
                    this.enemisTiles.push(tile);
                } else if (tile.type() === GameConstants.DungeonTile.boss || tile.type() === GameConstants.DungeonTile.ladder) {
                    this.bossTile = tile;
                }
            })
        })
    }

    static minDistance(nodes) {
        let min = Number.MAX_VALUE;
        let minIndex = undefined;

        nodes.forEach((node, index) => {
            if (node.d < min) {
                min = node.d;
                minIndex = index;
            }
        })
        return minIndex;
    }

    static updateDistances(node1, node2) {
        if (node2.d > node1.d + this.getTileWeight(node2)) {
            node2.d = node1.d + this.getTileWeight(node2);
            node2.previousNode = node1;
        }
    }

    static dijkstra(src) {
        //Copy nested array
        let nodes = [];

        // Initialize all node
        this.map.forEach((line) => {
            line.forEach((tile) => {
                nodes.push({
                    isVisible: tile.isVisible,
                    isAccessible: DungeonRunner.map.hasAccessToTile(tile.point),
                    type: tile.type(),
                    point: tile.point,
                    d: Number.MAX_VALUE - 1,
                    previousNode: undefined
                });
            });
        });

        // Distance of source is always 0
        let startNode = nodes.find(n => n.point.x === src.x && n.point.y === src.y);
        startNode.d = 0;

        //Compute node distance
        let toVisit = [...nodes];
        while (toVisit.length !== 0) {
            let minIndex = this.minDistance(toVisit);
            let node1 = toVisit[minIndex];
            toVisit.splice(minIndex, 1);
            this.nearbyTiles(nodes, node1.point).forEach((node2) => {
                this.updateDistances(node1, node2);
            });
        }

        //Select final node and path
        let path = [];
        let accessibleNode = nodes.filter(n => n.isAccessible);
        let finalNode = accessibleNode[this.minDistance(accessibleNode)];
        while (finalNode !== startNode) {
            path.push(finalNode);
            finalNode = finalNode.previousNode;
        }
        path.push(finalNode);
        return path;
    }

    static nearbyTiles(nodes, point) {
        return nodes.filter((node) => {
            return (node.point.x === point.x - 1 && node.point.y === point.y) ||
                (node.point.x === point.x + 1 && node.point.y === point.y) ||
                (node.point.x === point.x && node.point.y === point.y - 1) ||
                (node.point.x === point.x && node.point.y === point.y + 1);
        });
    }

    static getTileWeight(node) {
        if (!node.isVisible) {
            return 10;
        }
        switch (node.type) {
            case GameConstants.DungeonTile.empty:
            case GameConstants.DungeonTile.entrance:
            case GameConstants.DungeonTile.boss:
            case GameConstants.DungeonTile.ladder:
                return 1;
            case GameConstants.DungeonTile.chest:
                return 2;
            case GameConstants.DungeonTile.enemy:
                return 100;
            default:
                return 100;
        }
    }
}
class Quest {
    static run(config) {
        UI.changeQuestUI()
        let quests = App.game.quests.currentQuests();

        quests.forEach((quest, i) => {
            if (quest.isCompleted()) {
                return;
            }
            if (quest.route) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToRoute(quest.route, quest.region)
                })
            } else if (quest.dungeon) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToTown(quest.dungeon);
                    player.region = quest.region
                })
            } else if (quest.gymTown) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToTown(quest.gymTown);
                    player.region = quest.region
                })
            }
        });
    }
}class UI {
    static makeId(name) {
        return moduleName + capitalize(name);
    }

    static css() {
        $('head').append(`<style>  
        .btn-cell {
            display: flex;
            width: 100%;
        }

        #questDisplayBody .btn {
            flex: 1;
        } </style>`)
    }

    static build(config) {
        this.css();
        this.container(config)
        this.settingsModal(config);
        this.autoClicker(config);
    }

    static container(config) {
        let containerId = this.makeId('helperContainer');
        let containerBodyId = this.makeId('helperContainerBody');
        let roamingId = this.makeId('RoamingLocation');
        $('#left-column').append(
            `<div id="${containerId}" class="card sortable border-secondary mb-3" draggable="false" style=""> <div class="card-header p-0" data-toggle="collapse" href="#${containerBodyId}"> <span style="text-align:center">PokeHelper</span> </div> <div id="${containerBodyId}" class="card-body show p-0"> <table class="table table-sm m-0"><tbody>
                <tr><td class="p-0"><button id="${roamingId}" class="btn btn-primary btn-block" ${config.displayRoaming ? '' : 'style="display: none;"'}>Roaming pokemon not found</button></td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoBreeding', 'AutoBreeding', '', config.autoBreeding)}</td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoUnderground', 'AutoUnderground', '', config.autoUnderground)}</td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoRoute', 'AutoRoute', '', config.autoRoute)}</td></tr>
                <tr><td class="p-0"><button class="btn btn-primary btn-block" href="#pokeHelperSettingsModal" data-toggle="modal">Settings</button></td></tr>
            </tbody> </table> </div> </div>`
        )

        $(`#${containerId} button.toggleBtn`).on("click", (event) => {
            Main.handleToggle(event.target.name);
        });
        $(`#${roamingId}`).on("click", () => {
            Roaming.moveToRoaming();
        });
    }

    static addToggleButton(name, text, title, active) {
        return `<button class="toggleBtn  btn btn-block ${active ? 'btn-primary' : 'btn-secondary'}" id="${this.makeId(name)}" title="${title}" name="${name}">${text} ${active ? 'ON' : 'OFF'}</button>`
    }

    static toggleButton(name) {
        let button = $(`#${this.makeId(name)}`);
        button.toggleClass('btn-primary btn-secondary').removeClass('btn-danger');
        if (button.text().trim().endsWith("ON") || button.text().trim().endsWith("HALTED")) {
            button.text(button.text().replace('ON', 'OFF').replace('HALTED', 'OFF'));
        } else {
            button.text(button.text().replace('OFF', 'ON'));
        }
    }

    static settingsModal(config) {
        $('body').append(`
        <div class="modal noselect fade helperSettings" id="pokeHelperSettingsModal" tabindex="-1" style="z-index: 1040; display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="justify-content: space-around;">
                        <h5 class="modal-title">PokeHelper Settings</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    
                    <div class="modal-body p-0">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"><a class="nav-link active" href="#settings-general" data-toggle="tab">General</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-breeding" data-toggle="tab">Breeding</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-underground" data-toggle="tab">Underground</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-route" data-toggle="tab">Route</a></li>
                        </ul>
        
                        <div class="tab-content">
                            <div class="tab-pane active" id="settings-general">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addInputNumber('intervalSpeed', 'Reaction speed of the helper in ms, increase this to reduce the load on your processor:', '', config.intervalSpeed, 100)}
                                        </tr>
                                        <tr>
                                            ${this.addInputNumber('clickerIntervalSpeed', 'Time between click or movement in dungeon in ms, increase this to reduce the load on your processor:', '', config.clickerIntervalSpeed, 10)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('displayRoaming', 'Display the boosted location of the roaming pokemon:', '', config.displayRoaming)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('removeCatchTimer', 'Remove catching animation for pokeballs:', '', config.removeCatchTimer)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('dungeonClearChestFirst', 'Clear all chests before going to the boss in auto dungeon:', '', config.dungeonClearChestFirst)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('dungeonClearFightFirst', 'Clear all fight before going to the boss in auto dungeon:', '', config.dungeonClearFightFirst)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-breeding">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>    
                                            ${this.addInputText('breedingOrder', 'Breeding order:', '', config.breedingOrder, '')}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('breedOnlyNonShiny', 'Breed only pokemon not shiny:', '', config.breedOnlyNonShiny)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-underground">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addCheckBox('surveyFirst', 'Conduct survey before bomb:', config.surveyFirst)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-route">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addInputNumber('routeMaxDefeated', 'Number of pokemon to defeat before moving to the next route:', '', config.routeMaxDefeated, 1)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('routeOverride', 'Completely remove halt security on auto dungeon:', '', config.routeOverride)}
                                        </tr>
                                        <tr>
                                            ${this.addInputNumber('routeMinHaltPrice', 'Do not halt auto dungeon if price is below:', '', config.routeMinHaltPrice, 0)}
                                        </tr>
                                        <tr>
                                            ${this.addSelectBox('routeMinHaltRegion', 'Do not halt auto dungeon if region is or before:', '', config.routeMinHaltRegion, this.getRegion())}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        `);
        $(`#${this.makeId('breedingOrder')}`).on('keyup', (event) => {
            let isValid = Breeding.validateInput(event.target.value);
            if (isValid) {
                $(`#${this.makeId('breedingOrder')}`).addClass('is-valid').removeClass('is-invalid');
            } else {
                $(`#${this.makeId('breedingOrder')}`).addClass('is-invalid').removeClass('is-valid');
            }
        }).on('change', (event) => {
            Main.handleBreedingOrderInput(event.target.name, event.target.value);
        });
        $('#pokeHelperSettingsModal .checkBox').on('click', (event) => {
            Main.handleToggle(event.target.name);
        });
        $('#pokeHelperSettingsModal .inputNumber').on('change', (event) => {
            Main.handleNumberInput(event.target.name, event.target.value);
        });
        $('#pokeHelperSettingsModal .custom-select').on('change', (event) => {
            Main.handleSelectInput(event.target.name, event.target.value);
        });
    }

    static addInputText(name, text, title, value, pattern) {
        return ` 
        <td class="p-2">${text}</td>
        <td class="p-0">
            ${pattern ? '<form class="was-validated" onsubmit="return false">' : ''}
                <input class="form-control inputText is-valid" id="${this.makeId(name)}" type="text"
                       value="${value}" name="${name}"
                       pattern="${pattern}">
            ${pattern ? '</form>' : ''}
        </td>
        `;
    }

    static addInputNumber(name, text, title, value, min) {
        return `<td class="p-2">${text}</td>
                <td class="p-0">
                    ${min !== undefined ? '<form class="was-validated" onsubmit="return false">' : ''}
                        <input class="form-control inputNumber" id="${this.makeId(name)}" type="number"
                               value="${value}" name="${name}" ${min !== undefined ? `min="${min}"` : ''}>
                    ${min !== undefined ? '</form>' : ''}
                </td>`;
    }

    static addCheckBox(name, text, title, value) {
        return ` <td class="p-2">${text}</td>
                 <td class="p-2">
                     <input class="clickable checkBox" id="${this.makeId(name)}" type="checkbox" name="${name}"
                        ${value ? 'checked' : ''}>
                 </td>`;
    }

    static addSelectBox(name, text, title, value, choice) {
        return `<td class="p-2">${text}</td>
                <td class="p-0">
                    <select id="${this.makeId(name)}" name="${name}" class="custom-select" title="${title}">
                        ${this.objectToOption(choice, value)}
                    </select>
                </td>`;
    }

    static objectToOption(choice, selectedValue) {
        let options = ''
        Object.entries(choice).forEach((e) => {
            let id = Number(e[0])
            options += `<option value="${id}" ${id === selectedValue ? `selected='selected'`: ``}>${e[1]}</option>`
        })
        return options
    }

    static getRegion() {
        let regions = {};
        for (let i = -1; i <= player.highestRegion(); i++) {
            regions[i] = GameConstants.Region[i];
        }
        return regions
    }

    static autoClicker(config) {
        let tableId = this.makeId('clickerTable');
        $('.battle-view').before(`
        <table id="${tableId}"><tbody>
        <tr><td colspan="2">${this.addToggleButton('autoClicker', 'Auto Clicker', '', config.autoClicker)}</td></tr>
        <tr>
            <td style="width: 70%;">${this.addToggleButton('autoGym', 'Auto Gym', '', config.autoGym)}</td>
            <td>
                <select id="${this.makeId('selectedGym')}" class="btn btn-primary btn-block">
                    <option value="0">#1</option>
                    <option value="1">#2</option>
                    <option value="2">#3</option>
                    <option value="3">#4</option>
                    <option value="4">#5</option>
                </select>
            </td>
        </tr>
        <tr><td colspan="2">${this.addToggleButton('autoDungeon', 'Auto Dungeon', '', config.autoDungeon)}</td></tr>
        </tbody></table>`);

        $(`#${tableId} button.toggleBtn`).on("click", (event) => {
                let name;
                if (event.target.name) {
                    name = event.target.name;
                } else {
                    name = event.target.parentElement.name;
                }
                Main.handleToggle(name);
            }
        );
    }

    static changeQuestUI() {
        let buttons = $("#questDisplayBody > table > tbody > tr > td.p-0 > button.btn-block")
        buttons.removeClass("btn-block p-0")
        buttons.parent().addClass("btn-cell")
    }

    static addQuestButton(index, onClick) {
        let buttons = $(`#questDisplayBody > table > tbody:nth-child(1) > tr:nth-child(${(index + 1) * 2}) > td > button`)
        if (buttons.length === 1) {
            buttons.before(
                `<button class="btn btn-success btn-sm"> Go </button>`
            )
            buttons.prev().on('click', onClick);
        }
    }
}
class Main {
    static init() {
        this.config = Config.load();
        this.pokeballsCatchTime = [];
        UI.build(this.config);
        console.log("PokeClicker helper init done!");
        this.start();
    }

    static start() {
        if (this.mainInterval) {
            clearInterval(this.mainInterval)
        }
        this.mainInterval = setInterval(
            () => {
                Breeding.run(this.config);
                Underground.run(this.config);
                Roaming.run(this.config);
                Route.run(this.config);
                Clicker.run(this.config);
                Quest.run(this.config)
                this.run(this.config);
            }, this.config.intervalSpeed > 100 ? this.config.intervalSpeed : 500);
    }

    static handleToggle(name) {
        this.config[name] = !this.config[name];
        Config.save(this.config);
        UI.toggleButton(name, false);
    }

    static handleBreedingOrderInput(name, value) {
        if (!Breeding.validateInput(value)) {
            return
        }
        this.config[name] = value.split(',').map(Number);
        Config.save(this.config)
    }

    static handleNumberInput(name, value) {
        this.config[name] = Number(value);
        Config.save(this.config)
        if (name === 'intervalSpeed') {
            Main.start();
        } else if (name === 'clickerIntervalSpeed' && this.config.autoClicker && Clicker.timerId) {
            clearInterval(Clicker.timerId)
            Clicker.timerId = undefined
        }
    }

    static handleSelectInput(name, value) {
        this.config[name] = Number(value);
        Config.save(this.config)
    }

    static run(config) {
        if (config.removeCatchTimer && App.game.pokeballs.pokeballs[0].catchTime !== 0) {
            App.game.pokeballs.pokeballs.forEach((pokeball, i) => {
                this.pokeballsCatchTime[i] = pokeball.catchTime;
                pokeball.catchTime = 0
            });
        } else if (!config.removeCatchTimer && App.game.pokeballs.pokeballs[0].catchTime === 0) {
            App.game.pokeballs.pokeballs.forEach((pokeball, i) => {
                pokeball.catchTime = this.pokeballsCatchTime[i]
            });
        }
    }
}
let initInterval = setInterval(function () {
    if (!$('#game').hasClass('loading')) {
        Main.init();
        clearInterval(initInterval);
    }
}, 500);
