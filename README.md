# PokeClicker Helper

***
Multi tool to automate thing in Pokéclicker

## How to use

***
1. Have any user script manager (**not compatible with Greasemonkey 4**) install on your browser:
    * [Tampermonkey](https://www.tampermonkey.net/)
    * [Violentmonkey](https://violentmonkey.github.io/get-it/)
2. Click on the following link to trigger the installation of the script
    * [pokeclicker-helper.user.js](https://gitlab.com/Hiroa/pokeclicker-helper/-/raw/main/pokeclicker-helper.user.js)
3. Reload Pokéclicker

## Updating

***
Userscripts are set up to automatically update. You can check for updates from within the Greasemonkey or Tampermonkey
menu, or click on the install link again to force the update.

## Issues and feature request

***
Please post any issues or feature request within this
repository's [issue section](https://gitlab.com/Hiroa/pokeclicker-helper/-/issues).
