// ==UserScript==
// @name         PokeClicker helper
// @namespace    https://gitlab.com/Hiroa/pokeclicker-helper/
// @version      0.17.1
// @description  Multi tool script for pokéclicker (0.10.9)
// @author       Hiroa
// @updateURL    https://gitlab.com/Hiroa/pokeclicker-helper/-/raw/main/pokeclicker-helper.user.js
// @downloadURL  https://gitlab.com/Hiroa/pokeclicker-helper/-/raw/main/pokeclicker-helper.user.js
// @match        https://www.pokeclicker.com/*
// @icon         https://www.pokeclicker.com/assets/images/favicon.ico
// @require	     https://cdn.jsdelivr.net/npm/underscore@1.13.2/underscore-umd-min.js
// @grant        none
// ==/UserScript==

const moduleName = 'pokeClickerHelper';

const capitalize = (word) => word.charAt(0).toUpperCase() + word.slice(1);
