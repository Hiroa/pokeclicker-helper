class Route {

    static sortedGymList;

    static run(config) {
        if (!config.autoRoute || this.getDefeatedOnRoute(player.region, player.route()) < config.routeMaxDefeated) {
            return;
        }

        let route = this.getNextRoute(config);
        if (route) {
            MapHelper.moveToRoute(route.number, route.region);
        }
    }

    static shouldMoveToNextDungeon(config) {
        if (config.autoRoute) {
            if (player.town().dungeon !== undefined) {
                return this.getDefeatedOnDungeon(player.town().dungeon.name) >= config.routeMaxDefeated;
            } else if (this.getTownDungeon().length >= 1) {
                return this.getDefeatedOnDungeon(this.getTownDungeon()[0]) >= config.routeMaxDefeated;
            }
        }
        return false;
    }

    static moveToNextDungeon(config) {
        let dungeonName = this.getNextDungeonName(config);
        if (dungeonName) {
            MapHelper.moveToTown(dungeonName);
            player.region = TownList[dungeonName].region
        }
    }

    static shouldMoveToNextGym(currentGym, config) {
        if (config.autoRoute) {
            return this.getDefeatedOnGym(currentGym) >= config.routeMaxDefeated;
        }
        return false;
    }

    static moveToNextGym(config) {
        let gym = this.getNextGym(config);
        if (gym) {
            MapHelper.moveToTown(gym.parent.name);
            player.region = gym.parent.region
            this.handleAlolaSubRegion(gym)
            //Update selector
            let gymList = _.filter(gym.parent.content, c => {
                return c.badgeReward !== undefined
            });
            if (gymList.length > 1) {
                let index = _.findIndex(gymList, {town: gym.town});
                $(`#${UI.makeId('selectedGym')}`).val(index);
            }
        }
    }

    static getNextRoute(config) {
        let route = this.getNextRegionalRoute(player.region, config);
        if (route && MapHelper.accessToRoute(route.number, route.region)) {
            return route;
        }
        for (let i = 0; i <= player.highestRegion(); i++) {
            if (i === player.region) {
                continue;
            }
            let route = this.getNextRegionalRoute(i, config);
            if (route && MapHelper.accessToRoute(route.number, route.region)) {
                return route;
            }
        }
        return undefined;
    }

    static getNextGym(config) {
        let gym = this.getNextRegionalGym(player.region, config);
        if (gym && MapHelper.accessToTown(gym.parent.name)) {
            return gym;
        }
        for (let regionId = 0; regionId <= player.highestRegion(); regionId++) {
            if (regionId === player.region) {
                continue;
            }
            let gym = this.getNextRegionalGym(regionId, config);
            if (gym && MapHelper.accessToTown(gym.parent.name)) {
                return gym;
            }
        }
        return undefined;
    }

    static getNextDungeonName(config) {
        let dungeonName = this.getNextRegionalDungeon(player.region, config);
        if (dungeonName && MapHelper.accessToTown(dungeonName)) {
            return dungeonName;
        }
        for (let i = 0; i <= player.highestRegion(); i++) {
            if (i === player.region) {
                continue;
            }
            let dungeonName = this.getNextRegionalDungeon(i, config);
            if (dungeonName && MapHelper.accessToTown(dungeonName)) {
                return dungeonName;
            }
        }
        return undefined;
    }

    static getNextRegionalRoute(region, config) {
        let regionRoutes = Routes.getRoutesByRegion(region);
        return _.find(regionRoutes, (regionRoute) => this.getDefeatedOnRoute(region, regionRoute.number) < config.routeMaxDefeated);
    }

    static getNextRegionalGym(region, config) {
        if (!this.sortedGymList) {
            this.sortedGymList = _.indexBy(GymList, 'badgeReward')
        }
        let regionalGym = _.filter(this.sortedGymList, g => {
            return g.parent !== undefined && g.parent.region === region
        })
        return _.find(regionalGym, (gym) => this.getDefeatedOnGym(gym) < config.routeMaxDefeated && !gym.town.includes('Trial'));
    }

    static getNextRegionalDungeon(region, config) {
        let regionalDungeonName = GameConstants.RegionDungeons[region];
        return _.find(regionalDungeonName, (dungeonName) => this.getDefeatedOnDungeon(dungeonName) < config.routeMaxDefeated);
    }

    static getDefeatedOnRoute(region, route) {
        return App.game.statistics.routeKills[region][route]();
    }

    static getDefeatedOnGym(gym) {
        return App.game.statistics.gymsDefeated[GameConstants.getGymIndex(gym.town)]();
    }

    static getDefeatedOnDungeon(dungeonName) {
        return App.game.statistics.dungeonsCleared[GameConstants.getDungeonIndex(dungeonName)]();
    }

    static getCurrentTownGym() {
        let gymList = player.town().content.filter(c => {
            return c.badgeReward !== undefined
        });
        if (gymList.length === 1) {
            return gymList[0]
        } else if (gymList.length > 1) {
            let gymIndex = $(`#${UI.makeId('selectedGym')}`).val()
            return gymList[gymIndex];
        }
    }

    static getTownDungeon() {
        return player.town().content.filter(c => {
            return c.dungeon !== undefined
        }).map(c => {
            return c.dungeon
        });
    }

    static handleAlolaSubRegion(gym) {
        if (player.region === GameConstants.Region.alola) {
            let gymIndex = GameConstants.AlolaGyms.findIndex((name) => name === gym.town);
            if (gymIndex < 5) { //MelemeleIsland
                player.subregion = AlolaSubRegions.MelemeleIsland;
            } else if (gymIndex === 5) { //AkalaIsland
                player.subregion = AlolaSubRegions.AkalaIsland;
            } else if (gymIndex > 6) { //UlaulaAndPoniIslands
                player.subregion = AlolaSubRegions.UlaulaAndPoniIslands;
            } else if (gymIndex === 6 && player.subregion === 0) { //Aether Paradise
                player.subregion = AlolaSubRegions.AkalaIsland;
            }
            MapHelper.moveToTown(gym.parent.name); //Move again to counter the subregion change
        }
    }
}