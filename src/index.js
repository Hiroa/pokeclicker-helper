let initInterval = setInterval(function () {
    if (!$('#game').hasClass('loading')) {
        Main.init();
        clearInterval(initInterval);
    }
}, 500);
