class Main {
    static init() {
        this.config = Config.load();
        this.pokeballsCatchTime = [];
        UI.build(this.config);
        console.log("PokeClicker helper init done!");
        this.start();
    }

    static start() {
        if (this.mainInterval) {
            clearInterval(this.mainInterval)
        }
        this.mainInterval = setInterval(
            () => {
                Breeding.run(this.config);
                Underground.run(this.config);
                Roaming.run(this.config);
                Route.run(this.config);
                Clicker.run(this.config);
                Quest.run(this.config)
                this.run(this.config);
            }, this.config.intervalSpeed > 100 ? this.config.intervalSpeed : 500);
    }

    static handleToggle(name) {
        this.config[name] = !this.config[name];
        Config.save(this.config);
        UI.toggleButton(name, false);
    }

    static handleBreedingOrderInput(name, value) {
        if (!Breeding.validateInput(value)) {
            return
        }
        this.config[name] = value.split(',').map(Number);
        Config.save(this.config)
    }

    static handleNumberInput(name, value) {
        this.config[name] = Number(value);
        Config.save(this.config)
        if (name === 'intervalSpeed') {
            Main.start();
        } else if (name === 'clickerIntervalSpeed' && this.config.autoClicker && Clicker.timerId) {
            clearInterval(Clicker.timerId)
            Clicker.timerId = undefined
        }
    }

    static handleSelectInput(name, value) {
        this.config[name] = Number(value);
        Config.save(this.config)
    }

    static run(config) {
        if (config.removeCatchTimer && App.game.pokeballs.pokeballs[0].catchTime !== 0) {
            App.game.pokeballs.pokeballs.forEach((pokeball, i) => {
                this.pokeballsCatchTime[i] = pokeball.catchTime;
                pokeball.catchTime = 0
            });
        } else if (!config.removeCatchTimer && App.game.pokeballs.pokeballs[0].catchTime === 0) {
            App.game.pokeballs.pokeballs.forEach((pokeball, i) => {
                pokeball.catchTime = this.pokeballsCatchTime[i]
            });
        }
    }
}
