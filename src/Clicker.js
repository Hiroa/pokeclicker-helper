class Clicker {

    static timerId = undefined;
    static halted = false;

    static run(config) {
        if (config.autoClicker && !this.timerId) {
            clearInterval(this.timerId);
            this.timerId = setInterval(() => this.interval(config), config.clickerIntervalSpeed > 10 ? config.clickerIntervalSpeed : 50);
        } else if (!config.autoClicker && this.timerId) {
            clearInterval(this.timerId);
            this.timerId = undefined;
        }
    }

    static interval(config) {
        this.checkPreviousDungeonRun(config);
        switch (App.game.gameState) {
            case GameConstants.GameState.temporaryBattle:
                this.temporaryFighting()
                break;
            case GameConstants.GameState.fighting:
                this.fighting();
                break;
            case GameConstants.GameState.dungeon:
                this.removeHalt();
                this.dungeon(config);
                break;
            case GameConstants.GameState.gym:
                this.gym();
                break;
            case GameConstants.GameState.town:
                this.town(config);
                break;
        }
    }

    static fighting() {
        Battle.clickAttack();
    }

    static temporaryFighting() {
        TemporaryBattleBattle.clickAttack()
    }

    static dungeon(config) {
        if (!(DungeonRunner.fighting() || DungeonBattle.catching()) && config.autoDungeon) {
            DungeonCrawler.moveToNextTile(config)
        }
        switch (DungeonRunner.map.currentTile().type()) {
            case GameConstants.DungeonTile.boss:
                if ((config.dungeonClearChestFirst && DungeonCrawler.chestsTiles.length !== 0) ||
                    (config.dungeonClearFightFirst && DungeonCrawler.enemisTiles.length !== 0)) {
                    break;
                }
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.chest:
                if (config.autoDungeon && DungeonCrawler.bossTile.isVisible && !config.dungeonClearChestFirst) {
                    break;
                }
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.enemy:
            case GameConstants.DungeonTile.ladder:
                DungeonRunner.handleClick();
                break;
            case GameConstants.DungeonTile.empty:
                if (DungeonRunner.fighting()) {
                    DungeonRunner.handleClick();
                }
        }
    }

    static gym() {
        GymBattle.clickAttack();
    }

    static town(config) {
        //Auto Gym
        if (config.autoGym) {
            let gym = Route.getCurrentTownGym()
            if (gym) {
                if (Route.shouldMoveToNextGym(gym, config)) {
                    Route.moveToNextGym(config);
                } else {
                    GymRunner.startGym(gym);
                }
            }
        }

        //Auto Dungeon
        if (config.autoDungeon) {
            if (!this.halted && (player.town().dungeon || Route.getTownDungeon().length >= 1) && this.hasMoneyForTheDungeon()) {
                if (Route.shouldMoveToNextDungeon(config)) {
                    Route.moveToNextDungeon(config);
                } else {
                    if (player.town().dungeon) {
                        DungeonRunner.initializeDungeon(player.town().dungeon);
                    } else {
                        DungeonRunner.initializeDungeon(Route.getTownDungeon()[0]);
                    }
                }
            }
        }
    }

    static hasMoneyForTheDungeon() {

        if (player.town().dungeon) {
            return App.game.wallet.hasAmount(new Amount(player.town().dungeon.tokenCost, GameConstants.Currency.dungeonToken));
        } else {
            return App.game.wallet.hasAmount(new Amount(Route.getTownDungeon()[0].tokenCost, GameConstants.Currency.dungeonToken));
        }
    }

    static checkPreviousDungeonRun(config) {
        if (config.routeOverride) {
            return;
        }
        if (player.region <= config.routeMinHaltRegion) {
            return;
        }
        if (DungeonRunner.dungeon && DungeonRunner.dungeon.tokenCost < config.routeMinHaltPrice) {
            return;
        }
        //Check if previous run was successful
        if (config.autoDungeon && DungeonRunner.map && DungeonRunner.map.board() && DungeonRunner.dungeonFinished() && DungeonCrawler.bossStillAlive()) {
            let button = $(`#${UI.makeId('autoDungeon')}`);
            button.addClass('btn-danger');
            button.text(button.text().trim().replace('ON', 'HALTED'));
            this.halted = true;
        }
    }

    static removeHalt() {
        if (!this.halted) {
            return;
        }
        this.halted = false;
        let button = $(`#${UI.makeId('autoDungeon')}`);
        button.removeClass('btn-danger');
        button.text(button.text().trim().replace('HALTED', 'ON'));
    }
}
