class Config {
    static get default() {
        return {
            intervalSpeed: 500,
            clickerIntervalSpeed: 50,
            autoBreeding: false,
            autoUnderground: false,
            autoClicker: false,
            autoGym: false,
            autoDungeon: false,
            breedingOrder: [],
            breedOnlyNonShiny: false,
            surveyFirst: false,
            displayRoaming: true,
            removeCatchTimer: false,
            autoRoute: false,
            dungeonClearChestFirst: false,
            dungeonClearFightFirst: false,
            routeMaxDefeated: 10000,
            routeOverride: false,
            routeMinHaltRegion: -1,
            routeMinHaltPrice: 0
        };
    }

    static get storageKey() { return moduleName; }

    static load() {
        let config = window.localStorage.getItem(this.storageKey);
        if (!config) {
            this.save(this.default);
            return this.default;
        }
        return Object.assign(this.default, JSON.parse(config));
    }

    static save(config) {
        window.localStorage.setItem(this.storageKey, JSON.stringify(config));
    }
}
