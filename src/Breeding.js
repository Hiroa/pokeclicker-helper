class Breeding {

    static findPokemon(id) {
        return _.find(App.game.party.caughtPokemon, function (pokemon) {
            return pokemon.id === id;
        });
    }

    static validateInput(input) {
        let valid = true;
        let list = input.trim().split(',')
        list.every((number) => {
            let id = Number(number);
            if (PokemonHelper.getPokemonById(id).name === PokemonHelper.getPokemonById(0).name) {
                valid = false;
            }
            return valid;
        })
        return valid;
    }

    static run(config) {
        if (!config.autoBreeding) {
            return;
        }
        // let kantoHatcheryPriority = [130, 36, 40, 26, 35, 103, 59, 115, 128, 126, 89, 80, 114, 125, 127, 137, 123, 42, 55, 25, 39, 143, 108, 131, 110, 112, 22, 73, 38, 78, 97, 77, 121, 87, 83, 119, 91, 124, 76, 58, 49, 142];
        // let johtoHatcheryPriority = [184,234,203,241,206,176,233,217,199,224,211,164,232,214,229,222,198,168,210,226,221,171,185,193,192,130,195,207,169,205,162,178,200,215,227,202,173,174,160,181,230,212,216,240,157,237,231,219,154,186];
        // let hoennHatcheryPriority = [289,336,324,130,313,314,342,335,352,357,350,286,327,359,337,338,284,332,323,340,367,288,358,368,262,317,315,362,297,277,275,334,311,351,310,319,303,260,344,264,341,257,365,312,331,272,326];
        let maxSlot = App.game.breeding.eggSlots;
        for (let i = 0; i < maxSlot; i++) {
            let egg = App.game.breeding.eggList[i]();
            if (egg.progress() >= 100) {
                App.game.breeding.hatchPokemonEgg(i)
            }
            if (egg.type === -1) {
                let breedingId = config.breedingOrder.find((id) => {
                    let pokemon = Breeding.findPokemon(id);
                    if (pokemon === undefined || (config.breedOnlyNonShiny && pokemon.shiny)) {
                        return false;
                    }
                    return !pokemon.breeding && pokemon.level === 100;
                });
                if (breedingId !== undefined) {
                    let pokemon = this.findPokemon(breedingId);
                    App.game.breeding.addPokemonToHatchery(pokemon);
                }
            }
        }
    }
}
