class Quest {
    static run(config) {
        UI.changeQuestUI()
        let quests = App.game.quests.currentQuests();

        quests.forEach((quest, i) => {
            if (quest.isCompleted()) {
                return;
            }
            if (quest.route) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToRoute(quest.route, quest.region)
                })
            } else if (quest.dungeon) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToTown(quest.dungeon);
                    player.region = quest.region
                })
            } else if (quest.gymTown) {
                UI.addQuestButton(i, () => {
                    if (!DungeonRunner.dungeonFinished) {
                        return;
                    }
                    MapHelper.moveToTown(quest.gymTown);
                    player.region = quest.region
                })
            }
        });
    }
}