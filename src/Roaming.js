class Roaming {

    static run(config) {
        if (this.route !== RoamingPokemonList.increasedChanceRoute[player.region][0]().number) {
            this.route = RoamingPokemonList.increasedChanceRoute[player.region][0]().number;
            $(`#${UI.makeId('RoamingLocation')}`).text('Boosted roaming at route ' + this.route);
        }
        this.toggleDisplay(config);
    }

    static moveToRoaming() {
        if (this.route) {
            MapHelper.moveToRoute(RoamingPokemonList.increasedChanceRoute[player.region][0]().number, player.region);
        }
    }

    static toggleDisplay(config) {
        $(`#${UI.makeId('RoamingLocation')}`).toggle(config.displayRoaming);
    }
}