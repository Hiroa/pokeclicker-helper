class UI {
    static makeId(name) {
        return moduleName + capitalize(name);
    }

    static css() {
        $('head').append(`<style>  
        .btn-cell {
            display: flex;
            width: 100%;
        }

        #questDisplayBody .btn {
            flex: 1;
        } </style>`)
    }

    static build(config) {
        this.css();
        this.container(config)
        this.settingsModal(config);
        this.autoClicker(config);
    }

    static container(config) {
        let containerId = this.makeId('helperContainer');
        let containerBodyId = this.makeId('helperContainerBody');
        let roamingId = this.makeId('RoamingLocation');
        $('#left-column').append(
            `<div id="${containerId}" class="card sortable border-secondary mb-3" draggable="false" style=""> <div class="card-header p-0" data-toggle="collapse" href="#${containerBodyId}"> <span style="text-align:center">PokeHelper</span> </div> <div id="${containerBodyId}" class="card-body show p-0"> <table class="table table-sm m-0"><tbody>
                <tr><td class="p-0"><button id="${roamingId}" class="btn btn-primary btn-block" ${config.displayRoaming ? '' : 'style="display: none;"'}>Roaming pokemon not found</button></td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoBreeding', 'AutoBreeding', '', config.autoBreeding)}</td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoUnderground', 'AutoUnderground', '', config.autoUnderground)}</td></tr>
                <tr><td class="p-0">${this.addToggleButton('autoRoute', 'AutoRoute', '', config.autoRoute)}</td></tr>
                <tr><td class="p-0"><button class="btn btn-primary btn-block" href="#pokeHelperSettingsModal" data-toggle="modal">Settings</button></td></tr>
            </tbody> </table> </div> </div>`
        )

        $(`#${containerId} button.toggleBtn`).on("click", (event) => {
            Main.handleToggle(event.target.name);
        });
        $(`#${roamingId}`).on("click", () => {
            Roaming.moveToRoaming();
        });
    }

    static addToggleButton(name, text, title, active) {
        return `<button class="toggleBtn  btn btn-block ${active ? 'btn-primary' : 'btn-secondary'}" id="${this.makeId(name)}" title="${title}" name="${name}">${text} ${active ? 'ON' : 'OFF'}</button>`
    }

    static toggleButton(name) {
        let button = $(`#${this.makeId(name)}`);
        button.toggleClass('btn-primary btn-secondary').removeClass('btn-danger');
        if (button.text().trim().endsWith("ON") || button.text().trim().endsWith("HALTED")) {
            button.text(button.text().replace('ON', 'OFF').replace('HALTED', 'OFF'));
        } else {
            button.text(button.text().replace('OFF', 'ON'));
        }
    }

    static settingsModal(config) {
        $('body').append(`
        <div class="modal noselect fade helperSettings" id="pokeHelperSettingsModal" tabindex="-1" style="z-index: 1040; display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="justify-content: space-around;">
                        <h5 class="modal-title">PokeHelper Settings</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    
                    <div class="modal-body p-0">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"><a class="nav-link active" href="#settings-general" data-toggle="tab">General</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-breeding" data-toggle="tab">Breeding</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-underground" data-toggle="tab">Underground</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings-route" data-toggle="tab">Route</a></li>
                        </ul>
        
                        <div class="tab-content">
                            <div class="tab-pane active" id="settings-general">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addInputNumber('intervalSpeed', 'Reaction speed of the helper in ms, increase this to reduce the load on your processor:', '', config.intervalSpeed, 100)}
                                        </tr>
                                        <tr>
                                            ${this.addInputNumber('clickerIntervalSpeed', 'Time between click or movement in dungeon in ms, increase this to reduce the load on your processor:', '', config.clickerIntervalSpeed, 10)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('displayRoaming', 'Display the boosted location of the roaming pokemon:', '', config.displayRoaming)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('removeCatchTimer', 'Remove catching animation for pokeballs:', '', config.removeCatchTimer)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('dungeonClearChestFirst', 'Clear all chests before going to the boss in auto dungeon:', '', config.dungeonClearChestFirst)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('dungeonClearFightFirst', 'Clear all fight before going to the boss in auto dungeon:', '', config.dungeonClearFightFirst)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-breeding">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>    
                                            ${this.addInputText('breedingOrder', 'Breeding order:', '', config.breedingOrder, '')}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('breedOnlyNonShiny', 'Breed only pokemon not shiny:', '', config.breedOnlyNonShiny)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-underground">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addCheckBox('surveyFirst', 'Conduct survey before bomb:', config.surveyFirst)}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        
                            <div class="tab-pane" id="settings-route">
                                <table class="table table-striped table-hover m-0">
                                    <tbody>
                                        <tr>
                                            ${this.addInputNumber('routeMaxDefeated', 'Number of pokemon to defeat before moving to the next route:', '', config.routeMaxDefeated, 1)}
                                        </tr>
                                        <tr>
                                            ${this.addCheckBox('routeOverride', 'Completely remove halt security on auto dungeon:', '', config.routeOverride)}
                                        </tr>
                                        <tr>
                                            ${this.addInputNumber('routeMinHaltPrice', 'Do not halt auto dungeon if price is below:', '', config.routeMinHaltPrice, 0)}
                                        </tr>
                                        <tr>
                                            ${this.addSelectBox('routeMinHaltRegion', 'Do not halt auto dungeon if region is or before:', '', config.routeMinHaltRegion, this.getRegion())}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        `);
        $(`#${this.makeId('breedingOrder')}`).on('keyup', (event) => {
            let isValid = Breeding.validateInput(event.target.value);
            if (isValid) {
                $(`#${this.makeId('breedingOrder')}`).addClass('is-valid').removeClass('is-invalid');
            } else {
                $(`#${this.makeId('breedingOrder')}`).addClass('is-invalid').removeClass('is-valid');
            }
        }).on('change', (event) => {
            Main.handleBreedingOrderInput(event.target.name, event.target.value);
        });
        $('#pokeHelperSettingsModal .checkBox').on('click', (event) => {
            Main.handleToggle(event.target.name);
        });
        $('#pokeHelperSettingsModal .inputNumber').on('change', (event) => {
            Main.handleNumberInput(event.target.name, event.target.value);
        });
        $('#pokeHelperSettingsModal .custom-select').on('change', (event) => {
            Main.handleSelectInput(event.target.name, event.target.value);
        });
    }

    static addInputText(name, text, title, value, pattern) {
        return ` 
        <td class="p-2">${text}</td>
        <td class="p-0">
            ${pattern ? '<form class="was-validated" onsubmit="return false">' : ''}
                <input class="form-control inputText is-valid" id="${this.makeId(name)}" type="text"
                       value="${value}" name="${name}"
                       pattern="${pattern}">
            ${pattern ? '</form>' : ''}
        </td>
        `;
    }

    static addInputNumber(name, text, title, value, min) {
        return `<td class="p-2">${text}</td>
                <td class="p-0">
                    ${min !== undefined ? '<form class="was-validated" onsubmit="return false">' : ''}
                        <input class="form-control inputNumber" id="${this.makeId(name)}" type="number"
                               value="${value}" name="${name}" ${min !== undefined ? `min="${min}"` : ''}>
                    ${min !== undefined ? '</form>' : ''}
                </td>`;
    }

    static addCheckBox(name, text, title, value) {
        return ` <td class="p-2">${text}</td>
                 <td class="p-2">
                     <input class="clickable checkBox" id="${this.makeId(name)}" type="checkbox" name="${name}"
                        ${value ? 'checked' : ''}>
                 </td>`;
    }

    static addSelectBox(name, text, title, value, choice) {
        return `<td class="p-2">${text}</td>
                <td class="p-0">
                    <select id="${this.makeId(name)}" name="${name}" class="custom-select" title="${title}">
                        ${this.objectToOption(choice, value)}
                    </select>
                </td>`;
    }

    static objectToOption(choice, selectedValue) {
        let options = ''
        Object.entries(choice).forEach((e) => {
            let id = Number(e[0])
            options += `<option value="${id}" ${id === selectedValue ? `selected='selected'`: ``}>${e[1]}</option>`
        })
        return options
    }

    static getRegion() {
        let regions = {};
        for (let i = -1; i <= player.highestRegion(); i++) {
            regions[i] = GameConstants.Region[i];
        }
        return regions
    }

    static autoClicker(config) {
        let tableId = this.makeId('clickerTable');
        $('.battle-view').before(`
        <table id="${tableId}"><tbody>
        <tr><td colspan="2">${this.addToggleButton('autoClicker', 'Auto Clicker', '', config.autoClicker)}</td></tr>
        <tr>
            <td style="width: 70%;">${this.addToggleButton('autoGym', 'Auto Gym', '', config.autoGym)}</td>
            <td>
                <select id="${this.makeId('selectedGym')}" class="btn btn-primary btn-block">
                    <option value="0">#1</option>
                    <option value="1">#2</option>
                    <option value="2">#3</option>
                    <option value="3">#4</option>
                    <option value="4">#5</option>
                </select>
            </td>
        </tr>
        <tr><td colspan="2">${this.addToggleButton('autoDungeon', 'Auto Dungeon', '', config.autoDungeon)}</td></tr>
        </tbody></table>`);

        $(`#${tableId} button.toggleBtn`).on("click", (event) => {
                let name;
                if (event.target.name) {
                    name = event.target.name;
                } else {
                    name = event.target.parentElement.name;
                }
                Main.handleToggle(name);
            }
        );
    }

    static changeQuestUI() {
        let buttons = $("#questDisplayBody > table > tbody > tr > td.p-0 > button.btn-block")
        buttons.removeClass("btn-block p-0")
        buttons.parent().addClass("btn-cell")
    }

    static addQuestButton(index, onClick) {
        let buttons = $(`#questDisplayBody > table > tbody:nth-child(1) > tr:nth-child(${(index + 1) * 2}) > td > button`)
        if (buttons.length === 1) {
            buttons.before(
                `<button class="btn btn-success btn-sm"> Go </button>`
            )
            buttons.prev().on('click', onClick);
        }
    }
}
