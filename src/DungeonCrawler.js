class DungeonCrawler {

    static bossStillAlive() {
        return DungeonRunner.map.board()[DungeonRunner.map.playerPosition().floor].findIndex(line =>
            line.find(tile => {
                return tile.type() === GameConstants.DungeonTile.boss || tile.type() === GameConstants.DungeonTile.ladder
            })) !== -1
    }

    static moveToNextTile(config) {
        this.floor = DungeonRunner.map.playerPosition().floor
        this.map = DungeonRunner.map.board()[this.floor];
        this.chestsTiles = [];
        this.enemisTiles = [];
        this.bossTile = undefined;
        this.lookForTilesOfInterest();
        if (!this.bossTile) {
            return
        }
        let path;
        if (this.bossTile.isVisible
            && (!config.dungeonClearChestFirst || this.chestsTiles.length === 0)
            && (!config.dungeonClearFightFirst || this.enemisTiles.length === 0)) {
            path = this.dijkstra(this.bossTile.point)[0].point;
        } else if (config.dungeonClearChestFirst && this.chestsTiles.filter(t => t.isVisible).length !== 0) {
            path = this.chestsTiles.filter(t => t.isVisible)
                .map(chest => this.dijkstra(chest.point))
                .sort(((a, b) => a[0].d - b[0].d))[0][0].point;
        } else if (config.dungeonClearFightFirst && this.enemisTiles.filter(t => t.isVisible).length !== 0) {
            path = this.enemisTiles.filter(t => t.isVisible)
                .map(enemy => this.dijkstra(enemy.point))
                .sort(((a, b) => a[0].d - b[0].d))[0][0].point;
        } else {
            path = this.nextUnvisitedAccessibleTile();
        }
        if (path) {
            DungeonRunner.map.moveToCoordinates(path.x, path.y, this.floor);
        }
    }

    static nextUnvisitedAccessibleTile() {
        for (let y = this.map.length - 1; y >= 0; y--) {
            for (let x = this.map.length - 1; x >= 0; x--) {
                if (!this.map[y][x].isVisited && DungeonRunner.map.hasAccessToTile(new Point(x, y, this.floor))) {
                    return new Point(x, y, this.floor);
                }
            }
        }
    }

    static lookForTilesOfInterest() {
        this.chestsTiles = [];
        this.enemisTiles = [];
        this.map.forEach((line, y) => {
            line.forEach((tile, x) => {
                //Mark all tiles
                if (!tile.point) {
                    tile.point = new Point(x, y, this.floor);
                }
                if (tile.type() === GameConstants.DungeonTile.chest) {
                    this.chestsTiles.push(tile);
                } else if (tile.type() === GameConstants.DungeonTile.enemy) {
                    this.enemisTiles.push(tile);
                } else if (tile.type() === GameConstants.DungeonTile.boss || tile.type() === GameConstants.DungeonTile.ladder) {
                    this.bossTile = tile;
                }
            })
        })
    }

    static minDistance(nodes) {
        let min = Number.MAX_VALUE;
        let minIndex = undefined;

        nodes.forEach((node, index) => {
            if (node.d < min) {
                min = node.d;
                minIndex = index;
            }
        })
        return minIndex;
    }

    static updateDistances(node1, node2) {
        if (node2.d > node1.d + this.getTileWeight(node2)) {
            node2.d = node1.d + this.getTileWeight(node2);
            node2.previousNode = node1;
        }
    }

    static dijkstra(src) {
        //Copy nested array
        let nodes = [];

        // Initialize all node
        this.map.forEach((line) => {
            line.forEach((tile) => {
                nodes.push({
                    isVisible: tile.isVisible,
                    isAccessible: DungeonRunner.map.hasAccessToTile(tile.point),
                    type: tile.type(),
                    point: tile.point,
                    d: Number.MAX_VALUE - 1,
                    previousNode: undefined
                });
            });
        });

        // Distance of source is always 0
        let startNode = nodes.find(n => n.point.x === src.x && n.point.y === src.y);
        startNode.d = 0;

        //Compute node distance
        let toVisit = [...nodes];
        while (toVisit.length !== 0) {
            let minIndex = this.minDistance(toVisit);
            let node1 = toVisit[minIndex];
            toVisit.splice(minIndex, 1);
            this.nearbyTiles(nodes, node1.point).forEach((node2) => {
                this.updateDistances(node1, node2);
            });
        }

        //Select final node and path
        let path = [];
        let accessibleNode = nodes.filter(n => n.isAccessible);
        let finalNode = accessibleNode[this.minDistance(accessibleNode)];
        while (finalNode !== startNode) {
            path.push(finalNode);
            finalNode = finalNode.previousNode;
        }
        path.push(finalNode);
        return path;
    }

    static nearbyTiles(nodes, point) {
        return nodes.filter((node) => {
            return (node.point.x === point.x - 1 && node.point.y === point.y) ||
                (node.point.x === point.x + 1 && node.point.y === point.y) ||
                (node.point.x === point.x && node.point.y === point.y - 1) ||
                (node.point.x === point.x && node.point.y === point.y + 1);
        });
    }

    static getTileWeight(node) {
        if (!node.isVisible) {
            return 10;
        }
        switch (node.type) {
            case GameConstants.DungeonTile.empty:
            case GameConstants.DungeonTile.entrance:
            case GameConstants.DungeonTile.boss:
            case GameConstants.DungeonTile.ladder:
                return 1;
            case GameConstants.DungeonTile.chest:
                return 2;
            case GameConstants.DungeonTile.enemy:
                return 100;
            default:
                return 100;
        }
    }
}
