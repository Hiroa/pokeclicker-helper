class Underground {
    static run(config) {
        if (!config.autoUnderground) {
            return;
        }
        if (App.game.underground.energy + this.energyGain() > App.game.underground.getMaxEnergy()) {
            if (config.surveyFirst && !Mine.surveyResult()) {
                Mine.survey();
            } else {
                Mine.bomb();
            }
        }
    }

    static energyGain() {
        let oakMultiplier = App.game.oakItems.calculateBonus(OakItemType.Cell_Battery);
        return oakMultiplier * App.game.underground.getEnergyGain();
    }
}
